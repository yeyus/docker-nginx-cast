# About this Repo

This is the Git repo of the official Docker image for [nginx](https://registry.hub.docker.com/_/nginx/). See the
Hub page for the full readme on how to use the Docker image and for information
regarding contributing and issues.

The full readme is generated over in [docker-library/docs](https://github.com/docker-library/docs),
specificially in [docker-library/docs/nginx](https://github.com/docker-library/docs/tree/master/nginx).

### Launch

sudo docker run --name cast-nginx -d -p 80:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf:ro -v `pwd`/urlplayer:/etc/nginx/html/urlplayer -v /home/jesusft/Downloads:/etc/nginx/html/media nginx
